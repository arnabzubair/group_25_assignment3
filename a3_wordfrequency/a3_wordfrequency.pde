String[] lines; 
String[] words = new String[116];
String[] freq = new String[116];
void setup(){
  lines = loadStrings("wordfrequency.txt");
  for ( int i = 0; i < 116; i++){
    int count = 0;
    for (String retval:lines[i].split(":", 2)){
      if ( count == 0){
        freq[i] = retval;
        count++;
      } else {
        words[i]  = retval;
      }
    } 
         
  }      
  size(600,720);
  background(255);
  fill(191,87,0,125);
  rect(0,0,600,720);
  print(words[0]);
}
void draw(){
  fill(191,87,0);
  int yposition = 723;
  int newYaxis = width/2;
  rectMode(CENTER);
  for( int i = 0; i < 116; i++){
    yposition -= 6;
    int wordwidth = int(words[i])/2;
    int freqsize = 6;
    rect(newYaxis,yposition,wordwidth, freqsize);
}
}



//  for (int i = 0; i<height/h; i++) {
//    // Look at each key one at a time and retrieve its count.
//    String word = keys[i]; 
//    int count = concordance.get(word);

//    fill(51);
//    // Displaying a rectangle along with the count as a simple graph.
//    rect(0, i*20, count/4, h-4);
//    fill(0);
//    text(word + ": " + count, 10+count/4, i*h+h/2);
//    stroke(0);
//  }
