# import regular expressions
import re

# extract words function to open the .txt file and
# create a list of all words, with duplicates
def extract_words():
  in_file = open("alice.txt", "r",encoding = "utf8")
  text_string = in_file.read()
  in_file.close()

  text_string = text_string.lower()
  words = re.findall('[a-z]+', text_string)

  return words

# creates dictionary of words adding to the key count
# if not already represented
def count_words(word_list):

  word_count = {}
  
  for words in word_list:
    if words not in word_count:
      word_count[words] = 1
    else:
      word_count[words] += 1

  # remove contractions and common words in English
  contraction_list = ["s", "m", "re", "ve", "d", "ll", "t"]
  for contraction in contraction_list:
  	if contraction in word_count:
  		del word_count[contraction]

  common_words = ["the", "be", "to", "of", "and", "a", "in", \
  			      "that", "have", "i", "it", "for", "not", \
  			      "on", "with" "he", "as", "you", "do", "at", \
  			      "this", "but", "his", "by", "from", "they", \
  			      "we", "say", "her", "she", "said", "was", \
  			      "all", "with", "had", "so", "very", "he", \
  			      "were", "would", "or", "could", "did", "can", \
  			      "its", "are", "just", "their", "were", "then", \
  			      "is", "if", "what", "when", "them", "only", \
  			      "which", "get", "why", "been", "him", "come", \
  			      "will", "about", "well", "who", "my", "an", \
  			      "here", "there"]
  for word in common_words:
  	if word in word_count:
 	  del word_count[word]

  return word_count

# finds the frequency of all the words by adding 1 to the value
# if there is a duplicate word
def frequencies(dictionary):
  frequency_count = {}
  for key in dictionary:
    number_of_occurances = int(dictionary[key])
    if number_of_occurances not in frequency_count:
      frequency_count[number_of_occurances] = 1
    else:
      frequency_count[number_of_occurances] += 1

  return frequency_count

# main function
def main():
  all_words = extract_words()
  word_frequency = count_words(all_words)
  frequency_counts = frequencies(word_frequency)
  f = open("allwords.txt","w+")
  for i in all_words:
    f.write(i + "\n")
  f.close()
  g = open("uniquewords.txt","w+")
  for i in word_frequency:
    if word_frequency[i] == 1:
      g.write(i + "\n")
  g.close()
  h = open("wordfrequency.txt","w+")
  for i in sorted(frequency_counts):
    h.write(str(i) + ":" + str(frequency_counts[i]) + "\n")
  h.close()
  print("Alice's Adventures in Wonderland by Lewis Carroll\n")
  print("List of words sorted by frequencies:")
  print(sorted(word_frequency.items(), key=lambda x: x[1], reverse=True))

main()