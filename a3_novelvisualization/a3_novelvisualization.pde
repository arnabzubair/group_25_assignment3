// initialize global variables
String[] uniqueWords;
String word;
String textString;
PFont didot;
int[] colors = {#52307C, #7C5295, #B491C8};

// create 700x600 canvas and set font to Didot
void setup() {
  noLoop();
  size(700, 600);
  background(255);
  textLeading(10);
  didot = createFont("Didot", 20);
  textFont(didot);
  uniqueWords = loadStrings("uniquewords.txt");
}

void draw() {
  uniqueWords = loadStrings("uniquewords.txt");
  writeWords(uniqueWords, 0, 0);
}

// write words function
void writeWords(String[] textList, int x, int y) {
  for (int i = 0; i < textList.length; i++) {
    // obtains a random index from the text array and the color array
    int wordIndex = int(random(textList.length));
    int colorIndex = int(random(colors.length));
    fill(colors[colorIndex]);
    word = textList[wordIndex];
    // displays the words within the window constraints
    float textLength = textWidth(word);
    if (x + textLength >= width){
      x = 0;
      y += 19;
      if (y + 19 >= height){
        return;
      }
    }
    text(word, x, y, 700, 600);
    x += (10 + textLength);
  }
}
 
