Assignment 3 Description:

In the unique words visualization, we implemented the createFont and textFont functions to select the font Didot. Didot is a popular serif font that we thought would fit the theme of Alice's Adventures in Wonderland effectively. To wrap the text within the  display window, we utilized the text() function to prevent the  text from running over in the x and y directions.

For the word frequencies we implemented a simple distribution graph to display the number of occurances for each word. The program  utilizes centered rectangles to display the frequencies, with the  width of the rectangle being determined by the number of  occurances divided by 2, in order to better represent the outliers  of the graph.

For the Wordle word cloud, the cloud is in the shape of Alice from Disney's Alice in Wonderland animated film. We removed many of the most commonly occuring words in the English language, in order to prevent the cloud from being overcrowded with uninteresting words.

Arnab Zubair - Unique words visualization
Edward Flynn - Word frequency graph
Sean Mason - Wordle word cloud

We all worked together on extract_words.py.