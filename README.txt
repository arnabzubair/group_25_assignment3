How to run Assignment 3:
	
	1) Open the terminal and run:
		$ python extract_words.py

		This will create the files "allwords.txt", "wordfrequenct.txt", and "uniquewords.txt" for the novel, Alice's Adventures in Wonderland, by Lewis Carroll.

	2) Open the a3_novelvisualization directory and run the .pde file in Processing. This will create a randomly generated word cloud based on the unique words appearing only once in the novel.

	3) Open the a3_wordfrequency directory and run the .pde file in Processing. This will create a graph displaying the word frequencies with number of occurances in the x-axis and the words on y-axis.

	4) A Wordle word cloud is also in the directory, open "alicesadventuresinwonderland_wordle.png" to display a word cloud in the shape of the titular character Alice, which shows the most common words in the novel. 